//
//  ViewController.swift
//  music_player_demo
//
//  Created by XeVoNx on 30/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configSongs()
        table.delegate = self
        table.dataSource = self
    }
    
    
    func configSongs() {
        songs.append(Song(name: "w1", albumName: "al1", imageName: "med1", trackName: "w1", artistName: "ar1"))
        songs.append(Song(name: "w2", albumName: "al2", imageName: "med2", trackName: "w2", artistName: "ar2"))
        songs.append(Song(name: "w3", albumName: "al3", imageName: "med3", trackName: "w3", artistName: "ar3"))
        songs.append(Song(name: "w4", albumName: "al4", imageName: "med4", trackName: "w4", artistName: "ar4"))
        songs.append(Song(name: "w5", albumName: "al5", imageName: "med5", trackName: "w5", artistName: "ar5"))
        songs.append(Song(name: "61", albumName: "al6", imageName: "med6", trackName: "w6", artistName: "ar6"))
    }
    
    var songs = [Song]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let song = songs[indexPath.row]
        
        //config cell
        
        cell.accessoryType = .disclosureIndicator
        cell.detailTextLabel?.text = song.albumName
        cell.textLabel?.text = song.name
        cell.imageView?.image = UIImage(named: song.imageName)
        
        cell.textLabel?.font = UIFont(name: "Helvetica-Bold", size: 18)
        cell.detailTextLabel?.font = UIFont(name: "Helvetica", size: 17)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //present player
        var position = indexPath.row //to know which one is next
        
        guard let vc = storyboard?.instantiateViewController(identifier: "player") as?
            PlayerViewController else { return }
        
        vc.songs = songs
        vc.position = position
        //
        vc.changeBgFromPlayerBack = { result in
            position -= 1
            self.setBg(of: position)
        }
        vc.changeBgFromPlayerNext = { result in
            position += 1
            self.setBg(of: position)
        }
        
        //runs only when didSet (not in closures above)
        setBg(of: position)
        
        present(vc, animated: true)
    }

    func setBg(of row: Int = 0) {
        view.backgroundColor = [UIColor.red, UIColor.blue, UIColor.green, UIColor.yellow, UIColor.purple].randomElement()
        print("color of \(songs[row].imageName)")
    }
}

struct Song {
    let name: String
    let albumName: String
    let imageName: String
    let trackName: String
    let artistName: String
}

